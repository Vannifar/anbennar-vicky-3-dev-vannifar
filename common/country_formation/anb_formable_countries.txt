﻿B87 = { #Eordand
	use_culture_states = yes

	required_states_fraction = 0.75
	
	ai_will_do = { always = yes }

	possible = {
		any_country = {
			OR = {
				country_has_primary_culture = cu:peitar
				country_has_primary_culture = cu:selphereg
				country_has_primary_culture = cu:caamas
				country_has_primary_culture = cu:tuathak
				country_has_primary_culture = cu:snecboth
				country_has_primary_culture = cu:fograc
			}			
			has_technology_researched = pan-nationalism
		}
	}
}
B97 = { #Dolindha
	use_culture_states = yes

	required_states_fraction = 0.75
		
	ai_will_do = { always = yes }
}

C90 = { #Taychend
	use_culture_states = yes

	states = { 
		STATE_SARIHADDU 
		STATE_MESOKTI 
		STATE_URVAND 
		STATE_WEST_VEYII_SIKARHA 
		STATE_EAST_VEYII_SIKARHA 
		STATE_ORENKORAIM 
		STATE_DEGITHION 
		STATE_NYMBHAVA 
		STATE_KLERECHEND 
		STATE_NEOR_EMPKEIOS 
		STATE_NANRU_NAKAR 
		STATE_IRON_HILLS 
		STATE_ENLARMAI 
		STATE_CLEMATAR
	}

	required_states_fraction = 0.75
		
	ai_will_do = { always = yes }

	possible = {
		any_country = {
			has_technology_researched = pan-nationalism			
		}
	}
}

C91 = { #Theinos
	states = { 
 		STATE_ANISIKHEION 
		STATE_DEYEION 
		STATE_WESTERN_MTEIBHARA 
		STATE_EASTERN_MTEIBHARA
		STATE_APIKHOXI 
		STATE_AMGREMOS 
		STATE_BESOLAKI 
		STATE_ENEION 
		STATE_KEYOLION 
		STATE_EMPKEIOS 
		STATE_AMEION  
		STATE_VOLITHORAM 
		STATE_OKTIAMOTON  
		STATE_WESTERN_CHENDHYA
	}

	required_states_fraction = 0.75
		
	ai_will_do = { always = yes }

	possible = {
		any_country = {
			has_technology_researched = nationalism			
		}
	}
}

C92 = { #Ambhen Empire
	states = { 
		STATE_NYMBHAVA
		STATE_AMEION
		STATE_MESOKTI
		STATE_KLERECHEND
		STATE_NEOR_EMPKEIOS
		STATE_DEGITHION
		STATE_PARAIDAR
	}

	required_states_fraction = 0.70
		
	ai_will_do = { always = yes }

	possible = {
		any_country = {
			has_technology_researched = nationalism			
		}
	}
}

C93 = { #Chendhya
	use_culture_states = yes

	states = { 
		STATE_EMPKEIOS 
	}

	required_states_fraction = 0.65
		
	ai_will_do = { always = yes }

	possible = {
		any_country = {
			has_technology_researched = nationalism			
		}
	}
}

#Steadsman formable
G01 = {
	use_culture_states = yes

	required_states_fraction = 0.75
	
	ai_will_do = { always = yes }

	possible = {
		has_technology_researched = nationalism
	}
}

#United Expanse
G02 = {
	states = { 
		STATE_EBENMAS
		STATE_TELLUMTIR
		STATE_UZOO
		STATE_ELATHAEL
		STATE_ARANTAS
		STATE_CHIPPENGARD
		STATE_BEGGASLAND
		STATE_PLUMSTEAD
		STATE_BORUCKY
		STATE_ESIMOINE
		STATE_POSKAWA
	}
	required_states_fraction = 0.75

	possible = {
		has_technology_researched = nationalism
	}
	
	ai_will_do = { always = yes }
}

#Settler Veykoda
G03 = {
	use_culture_states = yes

	required_states_fraction = 0.8
	
	ai_will_do = { always = yes }

	possible = {
		has_technology_researched = nationalism
	}
}

#Autumnal League
G04 = {
	use_culture_states = yes

	required_states_fraction = 0.6

	ai_will_do = { always = yes }

	possible = {
		has_technology_researched = nationalism
	}
}

#Ynnic Empire
B66 = {
	states = {
		STATE_YRISRAD STATE_HRADAPOLERE STATE_VYCHVELS STATE_NIZVELS STATE_NIZELYNN STATE_YNNPADH STATE_EPADARKAN
		STATE_ARGANJUZORN STATE_MOCEPED STATE_USLAD STATE_ROGAIDHA STATE_VITREYNN STATE_BRELAR STATE_VERZEL
		STATE_JUZONDEZAN STATE_VIZKALADR STATE_POMVASONN STATE_GOMOSENGHA
	}

	required_states_fraction = 0.8

	ai_will_do = { always = yes }
}

#Dalaire Confederation
B65 = {
	use_culture_states = yes

	required_states_fraction = 0.75

	ai_will_do = { always = yes }
}

#Great Command
R21 = {
	states = {
		STATE_GHILAKHAD
		STATE_HOBGOBLIN_HOMELANDS
		STATE_RAGHAJANDI
		STATE_GHATASAK
		STATE_SHAMAKHAD_PLAINS
		STATE_SIR
		STATE_TUGHAYASA
		STATE_EAST_NADIMRAJ
		STATE_CENTRAL_NADIMRAJ
	}

	required_states_fraction = 0.7 #will have to fight *some* splinters outside of North Rahen

	ai_will_do = { always = yes }
}

#Haraz Dhumankon
D16 = {
	states = {
		STATE_ARG_ORDSTUN
		STATE_ORLGHELOVAR
		STATE_SHAZSTUNDIHR
		STATE_OVDAL_LODHUM
		STATE_VERKAL_SKOMDIHR
		STATE_GOR_BURAD
		STATE_ARGROD_TERMINUS
		STATE_ARGROD
		STATE_ARGROD_JUNCTION
		STATE_SCREAMING_CAVERNS
		STATE_DIAMOND_QUARRY
		STATE_WINDING_CAVERNS
		STATE_ORCSFALL
		STATE_FORGOTTEN_DEPTHS
		STATE_SCALDING_PITS
	}

	required_states_fraction = 0.8

	ai_will_do = { always = yes }
}

#Segbandal
D61 = {
	states = {
		STATE_SEGHDIHR
		STATE_SEGHROD
		STATE_VERKAL_GULAN
		STATE_HEHODOVAR
		STATE_OZUMROD
		STATE_GOR_OZUMBROG
		STATE_VAZUMROD
		STATE_GOR_VAZUMBROG
		STATE_CITRINE_MINES
		STATE_FORSAKEN_DEPTHS
		STATE_DRAKEDENS
		STATE_FOOLS_PASSAGE
		STATE_ANCIENT_DEPTHS
		STATE_RATHOLES
		STATE_GREATCAVERNS
	}

	required_states_fraction = 0.8

	ai_will_do = { always = yes }
}