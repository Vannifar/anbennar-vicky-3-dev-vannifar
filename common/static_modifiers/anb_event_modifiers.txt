﻿modifier_surveying_dhal_nikhuvad = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_negative.dds
 	country_bureaucracy_add = -1000
 }

 dhal_nikhuvad_purchase = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_expenses_add = 33000
}

dhal_nikhuvad_sale = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	country_tax_income_add = 33000
}

ghankedhen_increased_aristocratic_economic_activity = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	state_aristocrats_investment_pool_contribution_add = 0.1
}

modifier_triarchy_rending_repayments_1 = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_expenses_add = 3000
	country_prestige_mult = -0.1
	#state_radicals_from_sol_change_mult = 0.05 ## modifier does not exist
	country_gold_reserve_limit_mult = 0.5
}
modifier_tianlou_rending_repayments_1 = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	country_prestige_mult = 0.02
	country_tax_income_add = 3000
}
modifier_triarchy_rending_repayments_2 = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_expenses_add = 6000
	country_prestige_mult = -0.2
	#state_radicals_from_sol_change_mult = 0.1 ## modifier does not exist
	country_gold_reserve_limit_mult = 0.5
}
modifier_tianlou_rending_repayments_2 = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	country_tax_income_add = 6000
	country_prestige_mult = 0.02
}
modifier_triarchy_rending_repayments_3 = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_expenses_add = 9000
	country_prestige_mult = -0.3
	#state_radicals_from_sol_change_mult = 0.15 ## modifier does not exist
	country_gold_reserve_limit_mult = 0.5
}
modifier_tianlou_rending_repayments_3 = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	country_tax_income_add = 9000
	country_prestige_mult = 0.03
}
modifier_triarchy_rending_repayments_4 = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_expenses_add = 12000
	country_prestige_mult = -0.4
	#state_radicals_from_sol_change_mult = 0.2 ## modifier does not exist
	country_gold_reserve_limit_mult = 0.5
}
modifier_tianlou_rending_repayments_4 = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	country_tax_income_add = 12000
	country_prestige_mult = 0.04
}
modifier_cancelled_tianlou_debt = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_prestige_mult = -0.5
	country_infamy_decay_mult = -0.1
}
end_of_tianlou_repayments_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_negative.dds
	interest_group_approval_add = -2
}
the_jewel_of_yanshen_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_statue_positive.dds
	country_prestige_add = 60
}
survivors_of_the_fox_demon_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_positive.dds
	#state_radicals_from_sol_change_mult = -0.2 ## modifier does not exist
}
empkeios_independence_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_statue_positive.dds
	country_prestige_add = 10
}



dwarven_hold_specialization_change_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_negative.dds
	building_throughput_add = -0.75
	building_laborers_mortality_mult = 0.4
	building_machinists_mortality_mult = 0.2
}


obsidian_invasion_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_rifle_positive.dds
	country_tax_income_add = 7500
	country_bureaucracy_add = 250
	goods_input_small_arms_mult = -0.25
	goods_input_artillery_mult = -0.25
	country_infamy_generation_mult = -0.75
}

obsidian_invasion_barracks_training = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_rifle_positive.dds
	building_training_rate_add = 100000
}



doodad_production_modifier = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_gear_positive.dds
    building_doodad_manufacturies_throughput_add = 0.33
}