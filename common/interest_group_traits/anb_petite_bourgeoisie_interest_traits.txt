﻿ig_trait_self_reliance = {
	icon = "gfx/interface/icons/ig_trait_icons/tax_avoidance.dds"
	max_approval = unhappy
	
	modifier = {
		country_trade_route_quantity_mult = -0.25
	}
}