﻿ig_landowners = {
	color = hsv{ 0.63 0.40 0.69 }
	texture = "gfx/interface/icons/ig_icons/landowners.dds"
	layer = "revolution_dynamic_landowners"
	index = 4

	ideologies = {
		ideology_paternalistic
		ideology_stratocratic
		ideology_patriarchal
		ideology_machine_servitude
	}
	
	character_ideologies = {
		ideology_moderate
		ideology_traditionalist
	}

	enable = {
		always = yes
	}

	# Deprecated; use on_enable effect to assign traits instead
	traits = {
		ig_trait_noblesse_oblige
		ig_trait_family_ties
		ig_trait_noble_privileges
	}

	on_enable = {
		ig:ig_landowners = {
			#Ravelian State
			if = {
				limit = {
					owner = { c:A33 ?= this}
				}	#todo make our own
				set_ig_trait = ig_trait:ig_trait_noble_privileges
				set_ig_trait = ig_trait:ig_trait_family_ties
				set_ig_trait = ig_trait:ig_trait_noblesse_oblige
			}
			#Nahana Jadd
			else_if = {
				limit = {
					owner = { c:R01 ?= this}
				}	#todo make our own
				set_ig_trait = ig_trait:ig_trait_noble_privileges
				set_ig_trait = ig_trait:ig_trait_ministerial_duty
				set_ig_trait = ig_trait:ig_trait_the_fine_points_of_administration
			}
			else_if = {
				limit = {
					owner = { c:E01 ?= this}
				}
				set_ig_trait = ig_trait:ig_trait_noble_privileges
				set_ig_trait = ig_trait:ig_trait_societal_cohesion
				set_ig_trait = ig_trait:ig_trait_noblesse_oblige
			}
			else = {
				set_ig_trait = ig_trait:ig_trait_noble_privileges
				set_ig_trait = ig_trait:ig_trait_family_ties
				set_ig_trait = ig_trait:ig_trait_noblesse_oblige
			}
		}
		
		### Specific

		# Ravelian Rectorate Paternalistic
		if = {
			limit = {
				c:A33 ?= this
			}
			ig:ig_landowners = {
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_rectorate_paternalistic
			}
		}
		
		if = {
			limit = {
				c:E01 ?= this
			}
			ig:ig_landowners = {
				set_interest_group_name = ig_lake_stewards
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_isolationist
				add_ideology = ideology_triunic_stewards
			}
		}
		
		if = {
			limit = {
				c:C02 ?= this
			}
			ig:ig_landowners = {
				add_ideology = ideology_pro_slavery
			}
		}
		
		if = {
			limit = {
				c:C02 ?= this
			}
			ig:ig_landowners = {
				add_ideology = ideology_pro_slavery
			}
		}
		# Elven Havenist
		else_if = {
			limit = {
				OR = {
					c:A20 ?= this	#Ibevar
					c:A44 ?= this	#Moonhaven
					c:A65 ?= this	#Redglades
					c:A79 ?= this	#Cyranvar - can be changed in future to have their own
				}
			}
			ig:ig_landowners = {
				set_interest_group_name = ig_haven_elders
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_elven_havenist
			}
		}
		# Greentide Adventurers
		else_if = {
			limit = {
				OR = {
					c:A22 ?= this #Ancardia
					c:A24 ?= this #Newshire
					c:A25 ?= this #Araionn
					c:A26 ?= this #Wyvernheart
					c:A27 ?= this #Blademarches
					c:A28 ?= this #Rosande
					c:A30 ?= this #Magocratic Demesne
				}
			}
			ig:ig_landowners = {
				remove_ideology = ideology_stratocratic
				add_ideology = ideology_greentide_adventurers
			}
		}
		# Magocratic Paternalistic
		else_if = {
			limit = {
				OR = {
					c:A30 ?= this #Magocratic Demesne
				}
			}
			ig:ig_landowners = {
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_magocratic_paternalistic
			}
		}
		# Anbennar Baronite
		else_if = {
			limit = {
				OR = {
					c:A01 ?= this #Anbennar
				}
			}
			ig:ig_landowners = {
				set_interest_group_name = ig_baronites
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_baronites_paternalistic
			}
		}
		else_if = {
			limit = {
				OR = {
					c:B49 ?= this #Dragon Dominion
				}
			}
			ig:ig_landowners = {
				set_interest_group_name = ig_dragonblooded_nobility
			}
		}
		else_if = {
			limit = {
				OR = {
					c:A04 ?= this
				}
			}
			ig:ig_landowners = {
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_republican_paternalistic
			}
		}
		else_if = {
			limit = {
				any_primary_culture = {
					has_discrimination_trait = trollsbayer
				}
			}
			ig:ig_landowners = {
				set_interest_group_name = ig_planter_gentry
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_republican_paternalistic
			}
		}
		else_if = {
			limit = {
				any_primary_culture = {
					has_discrimination_trait = cannorian_heritage
					has_discrimination_trait = triarchic
				}
			}
			ig:ig_landowners = {
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_republican_paternalistic
			}
		}
		else_if = {
			limit = {
				any_primary_culture = {
					has_discrimination_trait = cannorian_heritage
					has_discrimination_trait = ynnic
				}
			}
			ig:ig_landowners = {
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_republican_paternalistic
			}
		}
		else_if = {
			limit = {
				any_primary_culture = {
					has_discrimination_trait = kheionai
				}
			}
			ig:ig_landowners = {
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_republican_paternalistic
			}
		}
		else_if = {
			limit = {
				OR = { #trade companies
					c:B01 ?= this
					c:B03 ?= this
					c:B04 ?= this
					c:B05 ?= this
				}
			}
			ig:ig_landowners = {
				remove_ideology = ideology_paternalistic
				remove_ideology = ideology_stratocratic
				add_ideology = ideology_republican_paternalistic
				add_ideology = ideology_oligarchic
			}
		}
		else_if = {
			limit = {
				any_primary_culture = {
					has_discrimination_trait = wuhyun
				}
			}
			ig:ig_landowners = {
				add_ideology = ideology_jingoist
			}
		}
		#Nahana Jadd Ministries
		else_if = {
			limit = {
				OR = {
					c:R01 ?= this
				}
			}
			ig:ig_landowners = {
				set_interest_group_name = ig_nahana_jadd_ministries
				remove_ideology = ideology_paternalistic
				add_ideology = ideology_raj_traditionalism
			}
		}
		#Ghankedheni Tribes
		else_if = {
			limit = {
				c:R02 ?= this
			}
			ig:ig_landowners = {
				set_interest_group_name = ig_ghankedhen_major_families
			}
		}
		if = {
			limit = {
				OR = {
					c:F13 ?= this
				}
			}
			ig:ig_landowners = {
				remove_ideology = ideology_patriarchal
				add_ideology = ideology_matriarchal
			}
		}
	}
	on_disable = {}
	on_character_ig_membership = {}

	pop_potential = {
		OR = {
			is_pop_type = aristocrats
			is_pop_type = clergymen
			is_pop_type = officers
			is_pop_type = farmers
			is_pop_type = adventurers	#anbennar. they are able to go in all, even these guys. think elite snobby rich ones
			is_pop_type = mages	#anbennar. mages are old guard like nobles - the status quo works for them
		}		
	}

	pop_weight = {
		value = 0

		#Anbennar
		add = {
			desc = "POP_MAGES"
			if = {
				limit = {
					is_pop_type = mages
				}
				value = 150
			}
		}


		add = {
			desc = "POP_ARISTOCRATS"
			if = {
				limit = {
					is_pop_type = aristocrats
				}
				value = 250
			}
		}

		add = {
			desc = "POP_CLERGYMEN"
			if = {
				limit = {
					is_pop_type = clergymen
				}
				value = 50
			}
		}

		add = {
			desc = "POP_OFFICERS"
			if = {
				limit = {
					is_pop_type = officers
				}
				value = 25
			}
		}

		add = {
			desc = "POP_FARMERS"
			if = {
				limit = {
					is_pop_type = farmers
				}
				value = 25
				add = this.standard_of_living
				
				if = {
					limit = { owner = { has_law = law_type:law_homesteading } }
					multiply = 1.25
				}

				if = {
					limit = {
						state = {
							is_slave_state = yes
						}
					}
					multiply = 4
				}
			}
		}

		multiply = {
			desc = "LEADER_POPULARITY"
			scope:interest_group = {
				leader ?= {
					value = popularity
					multiply = 0.0025
					add = 1
					max = 1.25
					min = 0.75
				}
			}
		}
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = yes
				}
			}
			multiply = {
				desc = "IN_GOVERNMENT_ATTRACTION"
				value = 1
				add = scope:interest_group.modifier:interest_group_in_government_attraction_mult
				min = 0
			}
		}
		
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = yes
				}
			}
			multiply = { # Multiplied by 1.25x max
				desc = "POP_LOYALISTS"
				value = pop_loyalist_fraction
				divide = 4
				add = 1
			}
		}
		if = {
			limit = {
				scope:interest_group = {
					is_in_government = no
				}
			}
			multiply = { # Multiplied by 1.25x max
				desc = "POP_RADICALS"
				value = pop_radical_fraction
				divide = 4
				add = 1
			}
		}
	}

	monarch_weight = {
		value = 1.5 # inherently likely
		# Monarch is not likely to adopt a marginal IG
		if = {
			limit = {
				is_marginal = yes
			}
			multiply = {
				value = 0.1
			}
		}
		# Monarch is more likely to adopt a Powerful IG
		if = {
			limit = {
				is_powerful = yes
			}
			multiply = {
				value = 2
			}
		}
		# More likely with Serfdom or Slavery
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_serfdom
						has_law = law_type:law_slave_trade
						has_law = law_type:law_legacy_slavery
					}
				}
			}
			multiply = {
				value = 2
			}
		}
		# More likely with Autocracy, Oligarchy, or Landed Voting
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_oligarchy
						has_law = law_type:law_autocracy
						has_law = law_type:law_landed_voting
					}
				}
			}
			multiply = {
				value = 2
			}
		}
	}

	agitator_weight = {
		# Agitators more likely to come from populist IG's rather than elitist
		value = 0.25
		# Agitators will never be part of a marginalized IG
		if = {
			limit = {
				owner.ig:ig_landowners = {
					is_marginal = yes
				}
			}
			multiply = {
				value = 0
			}
		}
	}

	commander_weight = {
		value = 2.0
	}

	noble_chance = {
		value = 1.0
	}

	female_commander_chance = {
		value = 0.0
		#Anbennar
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					NOT = { has_law = law_type:law_womens_suffrage }
				}
			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}
			}
			add = {
				value = 0.50
			}
		}
		if = {
			limit = {
				owner = {
					has_technology_researched = tradition_of_equality
				}
			}
			add = {
				value = 0.50
			}
		}
	}

	female_politician_chance = {
		value = 0.0
		#Anbennar
		if = {
			limit = {
				owner = { #variable from country history gets set after IGs being loaded, this need to be done for it to work
					OR = {
						c:F13 ?= this
					}
					NOT = { has_law = law_type:law_womens_suffrage }
				}

			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}

			}
			add = {
				value = 0.97
			}
		}
		if = {
			limit = { 
				owner = {
					has_technology_researched = tradition_of_equality
				}

			}
			add = {
				value = 0.50
			}
		}
		if = {
			limit = {
				owner = {
					has_law = law_type:law_womens_suffrage
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.01
			}
		}
	}

	female_agitator_chance = {
		value = 0.01
		#Anbennar
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					NOT = { has_law = law_type:law_womens_suffrage }
				}

			}
			add = {
				value = 1.0
			}
		}
		if = {
			limit = {
				owner = {
					has_variable = is_matriarchy_var
					has_law = law_type:law_womens_suffrage
				}

			}
			add = {
				value = 0.96
			}
		}
		if = {
			limit = {
				owner = {
					has_technology_researched = tradition_of_equality
				}

			}
			add = {
				value = 0.49
			}
		}
		if = {
			limit = {
				owner = {
					has_law = law_type:law_women_own_property
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.01
			}
		}	
		
		if = {
			limit = {
				owner = {
					has_law = law_type:law_women_in_the_workplace
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.01
			}
		}

		if = {
			limit = {
				owner = {
					has_law = law_type:law_womens_suffrage
					NOT  = { has_technology_researched = tradition_of_equality } #Anbennar
				}

			}
			add = {
				value = 0.01
			}
		}		
	}

	# The chance that a commander belonging to this IG takes over leadership when it changes
	# scope:character is the most popular commander in the IG
	commander_leader_chance = {
		value = 0.5
		multiply = ig_commander_leader_chance_mult
	}
}