﻿COUNTRIES = {
	c:B52 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement

		effect_starting_politics_traditional = yes

		activate_law = law_type:law_oligarchy #Feudal nobility

		activate_law = law_type:law_censorship
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_traditional_magic_only
		activate_law = law_type:law_pragmatic_artifice #Bloodrunes

		set_ruling_interest_groups = {
			ig_landowners
		}
	}
}
