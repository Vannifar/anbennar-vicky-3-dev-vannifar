﻿COUNTRIES = {
	c:A31 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy # These two were chiefdom and elder council, but that isn't allowed for centralized nation
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_right_of_assembly	#orcish shizz, if you're that strong then people should be free to challenge
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_in_the_workplace # Not allowed women in workplace without feminism
		activate_law = law_type:law_legacy_slavery

		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_traditional_magic_only

		add_taxed_goods = g:grain
		add_taxed_goods = g:meat
	}
}