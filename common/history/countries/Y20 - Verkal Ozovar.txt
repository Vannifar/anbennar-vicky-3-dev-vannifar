﻿COUNTRIES = {
	c:Y20 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = urban_planning
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		
		effect_starting_politics_traditional = yes
	}
}