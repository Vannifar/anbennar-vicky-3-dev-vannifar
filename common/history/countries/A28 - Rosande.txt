﻿COUNTRIES = {
	c:A28 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = tradition_of_equality
		
		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_agrarianism
		
		activate_law = law_type:law_women_in_the_workplace
		activate_law = law_type:law_slave_trade

		activate_law = law_type:law_traditional_magic_only	#mages allowed, wasnt hit by verdancy as they refused
		
		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
	}
}