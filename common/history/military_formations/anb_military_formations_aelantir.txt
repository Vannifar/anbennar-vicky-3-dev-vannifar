﻿MILITARY_FORMATIONS = {
	c:B85 ?= { # Murdkather
		create_military_formation = {
			type = army
			hq_region = sr:region_eordand
			name = "Summer's Vanguard"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MURDKATHER
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_PASKALA
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_DEARKTIR
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_TRIMTHIR
				count = 2
			}
			save_scope_as = summer_vanguard
		}

		#create_character = {
		#	template = B85_general
		#	save_scope_as = B85_gen
		#}

		#scope:B85_gen = {
		#	transfer_to_formation = scope:summer_vanguard
		#}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_eordand
			name = "Summer Breeze"
		
			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_TRIMTHIR
				count = 2
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_PASKALA
				count = 2
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_MURDKATHER
				count = 5
			}
		}
	}
	c:B84 ?= { # Arakeprun
		create_military_formation = {
			type = army
			hq_region = sr:region_eordand
			name = "Spring Army"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DARHAN
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_TRASAND
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GATHGOB
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_PELODARD
				count = 2
			}
			save_scope_as = spring_army
		}
	}
	c:B82 ?= { # Einnsag
		create_military_formation = {
			type = army
			hq_region = sr:region_eordand
			name = "Autumnnal Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_EINNSAG
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_RAITHLOS
				count = 2
			}
			save_scope_as = autumnal_army
		}
	}
	c:B81 ?= { # Eighard
		create_military_formation = {
			type = army
			hq_region = sr:region_eordand
			name = "Autumnnal Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_FOGHARBAC
				count = 6
			}
			save_scope_as = autumnal_army
		}
	}
	c:B83 ?= { # Sglard
		create_military_formation = {
			type = army
			hq_region = sr:region_eordand
			name = "Autumnnal Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SGLOLAD
				count = 3
			}
			save_scope_as = autumnal_army
		}
	}
	c:B80 ?= { # Eordand Admin
		create_military_formation = {
			type = army
			hq_region = sr:region_eordand
			name = "Occupation Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_DARTIR
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_GALBHAN
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_JHORGASHIRR
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GEMRADCURT
				count = 3
			}
			save_scope_as = occupation_army
		}
	}
	c:B49 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_upper_ynn
			name = "Bozamachi"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_VIZKALADR
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GOMOSENGHA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_POMVASONN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_JUZONDEZAN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_VERZEL
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_POMVASONN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_VIZKALADR
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_EKRSOKA
				count = 2
			}
            save_scope_as = B49_army
		}
		create_character = {
				is_general = yes
				save_scope_as = B49_gen
			}
		scope:B49_gen = {
            transfer_to_formation = scope:B49_army
		}
		create_military_formation = {
			type = army
			hq_region = sr:region_upper_ynn
			name = "Dragon Knights"
	
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BRELAR
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_JUZONDEZAN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MOCEPED
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_ROGAIDHA
				count = 8
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_VERZEL
				count = 1
			}
			save_scope_as = B49_army_2
		}

		save_scope_as = B49_army_2
			create_character = {
					is_general = yes
					save_scope_as = B49_gen_2
				}
			scope:B49_gen_2 = {
				transfer_to_formation = scope:B49_army_2
		}
	}
	c:B91 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_epednan_expanse

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_EBENMAS
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_lancers
				state_region = s:STATE_EBENMAS
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_TELLUMTIR
				count = 8
			}
			combat_unit = {
				type = unit_type:combat_unit_type_lancers
				state_region = s:STATE_TELLUMTIR
				count = 4
			}
		}
	}
	c:B37 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_epednan_expanse

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ELATHAEL
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_lancers
				state_region = s:STATE_ELATHAEL
				count = 2
			}
		}
	}
	c:B34 ={
		create_military_formation = {
			type = army
			hq_region = sr:region_epednan_expanse

			combat_unit = {
		    type = unit_type:combat_unit_type_irregular_infantry
			state_region = s:STATE_BEGGASLAND
			count = 6
		    }
			combat_unit = {
		    type = unit_type:combat_unit_type_irregular_infantry
			state_region = s:STATE_NIZELYNN
			count = 2
		    }
		}
	}
	c:B41 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_epednan_expanse

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_PLUMSTEAD
				count = 14
		    }
			combat_unit = {
				type = unit_type:combat_unit_type_lancers
				state_region = s:STATE_PLUMSTEAD
				count = 3
			}
		}
	}
	c:B42 ={
		create_military_formation = {
			type = army
			 hq_region = sr:region_epednan_expanse

			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_ESIMOINE
			count = 4
		    }
			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_TUSNATA
			count = 2
		    }
			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_BORUCKY
			count = 2
		    }
		}
	}
	c:B30 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_epednan_expanse

			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_CHIPPENGARD
			count = 10
		    }
		}
	}
	c:B45 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lower_ynn

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_EPADARKAN
				count = 6
		    }
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_EPADARKAN
				count = 1
			}
		}
	}
	c:B46 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lower_ynn

			combat_unit = {
		    type = unit_type:combat_unit_type_irregular_infantry
			state_region = s:STATE_EPADARKAN
			count = 4
		    }
		}
	}
	c:B29 ?= { #sarda empire
		create_military_formation = {
			type = army
			hq_region = sr:region_lower_ynn

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_HRADAPOLERE
				count = 8
		    }
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_HRADAPOLERE
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_NIZVELS
				count = 8
		    }
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_NIZVELS
				count = 5
			}
			combat_unit = {
		    type = unit_type:combat_unit_type_irregular_infantry
			state_region = s:STATE_YNNPADH
			count = 3
		    }
			combat_unit = {
		    type = unit_type:combat_unit_type_irregular_infantry
			state_region = s:STATE_VYCHVELS
			count = 7
		    }
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_YRISRAD
				count = 10
		    }
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_YRISRAD
				count = 5
			}
		}
	}
	c:B31 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lower_ynn

			combat_unit = {
		    type = unit_type:combat_unit_type_irregular_infantry
			state_region = s:STATE_LETHIR
			count = 4
		    }
			combat_unit = {
		    type = unit_type:combat_unit_type_irregular_infantry
			state_region = s:STATE_VIZANIRZAG
			count = 2
		    }
		}
	}
	c:B32 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lower_ynn

			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_WEST_TIPNEY
			count = 4
		    }
		}
	}
	c:B33 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lower_ynn

			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_CORINSFIELD
			count = 7
		    }
		}
	}
	c:B36 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lower_ynn

			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_ARGEZVALE
			count = 10
		    }
		}
	}
	c:B35 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lower_ynn

			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_OSINDAIN
			count = 2
		    }
			combat_unit = {
		    type = unit_type:combat_unit_type_line_infantry
			state_region = s:STATE_NEW_HAVORAL
			count = 5
		    }
		}
	}
	c:B47 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_upper_ynn

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_ARGANJUZORN
				count = 9
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_ARGANJUZORN
				count = 3
			}
		}
	}
	c:B52 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_upper_ynn

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_USLAD
				count = 7
			}
		}
	}
	c:B39 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_upper_ynn

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_USLAD
				count = 6
			}
		}
	}
	c:B38 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_upper_ynn

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_VITREYNN
				count = 8
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_VITREYNN
				count = 2
			}
		}
	}
	c:B53 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_upper_ynn

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_VARBUKLAND
				count = 6
			}
		}
	}
	c:B61 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_dalaire

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HJORDAL
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GROONCAMB
				count = 2
			}
			
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HJORDAL
				count = 4
			}
		}
	}
	c:B27 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_noruin

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DARKGROVES
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CHESHOSHMAR
				count = 8
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GRAVEROADS
				count = 2
			}
		}
	}
	c:B26 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_noruin

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_OKHIBOLI
				count = 7
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_OKHIBOLI
				count = 1
			}
		}
	}
	c:B98 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_noruin
			name = "Central Concord Command"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_VALORPOINT
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_THILVIS
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ISOBELIN
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ZANLIB
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SOUTH_MARLLIANDE
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_TROLLSBRIDGE
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CESTIRMARK
				count = 4
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_noruin

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_CESTIRMARK
				count = 3
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_SOUTH_MARLLIANDE
				count = 3
			}

			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_TROLLSBRIDGE
				count = 1
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_ZANLIB
				count = 5
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_ISOBELIN
				count = 5
			}

			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_THILVIS
				count = 3
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_VALORPOINT
				count = 6
			}
		}
	}
	c:B18 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_noruin

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_KWINELLIANDE
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_NURTHANNCOST
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SOUTH_DALAIRE
				count = 1
			}
		}
	}
	c:B89 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_noruin

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BOEK
				count = 1
			}
		}
	}
	c:B21 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_noruin

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_YNNSMOUTH
				count = 5
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_noruin

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_YNNSMOUTH
				count = 2
			}
		}
	}

	c:B15 ?= {
		create_military_formation = {
			type = fleet
			hq_region = sr:region_ruined_circle

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_SILVERTENNAR
				count = 5
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_FALITCOST
				count = 5
			}
		}
	}

	c:B01 ?= {
		create_military_formation = {
			type = fleet
			hq_region = sr:region_ruined_circle

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_MOUTH_OF_RUIN
				count = 8
			}

			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_MOUTH_OF_RUIN
				count = 2
			}
		}
	}

	c:B17 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_dalaire

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_DALAIREY_WASTES
				count = 1
			}
		}
	}
	c:B14 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_ruined_circle

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CAPE_OF_ENDRAL
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_IBSEAN
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_RUBENSHORE
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_JERCEL
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MARBELOCH
				count = 3
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_ruined_circle

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_CAPE_OF_ENDRAL
				count = 8
			}

			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_CAPE_OF_ENDRAL
				count = 2
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_RUBENSHORE
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_JERCEL
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_MARBELOCH
				count = 5
			}
		}
	}
    c:B07 ?= { #Triarchy
        create_military_formation = {
            type = army
            hq_region = sr:region_haraf

            combat_unit = {
                type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GOOVRAZ
				count = 2
            }
            combat_unit = {
                type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GOMMIOCHAND
				count = 5
            }
            combat_unit = {
                type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MESTIKARDU
				count = 5
            }
            combat_unit = {
                type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ZURZUMEXIA
				count = 5
            }
            combat_unit = {
                type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_GOOVRAZ
				count = 8
            }
            combat_unit = {
                type = unit_type:combat_unit_type_lancers
				state_region = s:STATE_GOMMIOCHAND
				count = 5
            }
        }
        create_military_formation = {
            type = fleet
            hq_region = sr:region_haraf

            combat_unit = {
                type = unit_type:combat_unit_type_frigate
                state_region = s:STATE_GOMMIOCHAND
                count = 4
            }
            combat_unit = {
                type = unit_type:combat_unit_type_frigate
                state_region = s:STATE_MESTIKARDU
                count = 2
            }
            combat_unit = {
                type = unit_type:combat_unit_type_frigate
                state_region = s:STATE_ZURZUMEXIA
                count = 3
            }
            combat_unit = {
                type = unit_type:combat_unit_type_man_o_war
                state_region = s:STATE_ZURZUMEXIA
                count = 1
            }
            combat_unit = {
                type = unit_type:combat_unit_type_man_o_war
                state_region = s:STATE_GOMMIOCHAND
                count = 2
            }
        }
    }
    c:B09 ?= { #Minaria
        create_military_formation = {
            type = army
            hq_region = sr:region_tor_nayyi

            combat_unit = {
                type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MINAR_YOLLI
				count = 5
            }
            combat_unit = {
                type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_MANGROVY_COAST
				count = 2
            }
        }
    }
    c:B10 ?= { #Haraf
        create_military_formation = {
            type = army
            hq_region = sr:region_haraf

            combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_NANI_NOLIHE
				count = 8
			}
            combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_NIDI_BIKEHA
				count = 8
			}
            combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_AUVUL_TADIH
				count = 4
			}
        }
    }
	c:C03 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_soruin

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MARUKHAN
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_OZGAR
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BRAMMYAR
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_FASHTUG
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_YARUHOL
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_NOGRUD
				count = 3
			}
		}
	}
	c:C04 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_soruin

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_JIBIRAEN
				count = 5
			}
		}
	}
	c:C06 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_effelai

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SORFEN
				count = 8
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_THE_MIDDANS
				count = 2
			}
		}
	}
	c:C05 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lai_peninsula

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SEINAINE
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DALAINE
				count = 6
			}
		}
	}
	c:C08 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lai_peninsula

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_THALASARAN
				count = 5
			}
		}
	}
	c:C07 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lai_peninsula

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KIOHALEN
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KIINDTIR
				count = 4
			}
		}
	}
	c:C09 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lai_peninsula

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_EAST_TURTLEBACK
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_WEST_TURTLEBACK
				count = 2
			}
		}
	}
	c:C01 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lai_peninsula

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DAZINKOST
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_REZANOAN
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BROAN_KEIR
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_NUREL
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ARENEL
				count = 4
			}
		}
	}
	c:C10 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_lai_peninsula

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_VRENDIN
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_TIMBERNECK
				count = 1
			}
		}
	}
	c:C02 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_amadia

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CYMBEAHN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ARANLAS
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BRONRIN
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SILVEGOR
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CLAMGUINN
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CARA_LAFQUEN
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_OOMU_NELIR
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_NUR_ISTRALORE
				count = 2
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_amadia

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_BRONRIN
				count = 2
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_SILVEGOR
				count = 2
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_CYMBEAHN
				count = 4
			}
		}
	}
	c:C11 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_amadia

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HARENAINE
				count = 2
			}
		}
	}
	c:C16 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_effelai

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_MUSHROOM_FOREST
				count = 5
			}
		}
	}
	c:B05 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_tor_nayyi

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_STEEL_BAY
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GREENHILL
				count = 5
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_tor_nayyi

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_STEEL_BAY
				count = 10
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_GREENHILL
				count = 2
			}
		}
	}

	c:G05 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_dalaire

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GROONCAMB
				count = 2
			}
		}
	}

    c:B59 ?= { #Themaria
        create_military_formation = {
            type = army
            hq_region = sr:region_broken_sea

            combat_unit = {
                type = unit_type:combat_unit_type_line_infantry
                state_region = s:STATE_TRITHEMAR
                count = 5
            }
        }
        create_military_formation = {
            type = fleet
            hq_region = sr:region_broken_sea

            combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_TRITHEMAR
				count = 3
			}
        }
    }

    c:B62 ?= { #Noo Coddoran
        create_military_formation = {
            type = army
            hq_region = sr:region_broken_sea

            combat_unit = {
                type = unit_type:combat_unit_type_line_infantry
                state_region = s:STATE_SILDARBAD
                count = 3
            }
        }
    }

    c:B60 ?= { #Nortiochand
        create_military_formation = {
            type = army
            hq_region = sr:region_broken_sea

            combat_unit = {
                type = unit_type:combat_unit_type_line_infantry
                state_region = s:STATE_MITTANWEK
                count = 2
            }
        }
    }

}