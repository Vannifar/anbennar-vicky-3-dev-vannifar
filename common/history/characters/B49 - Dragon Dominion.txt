﻿CHARACTERS = {
	c:B49 ?= {
		create_character = {
			first_name = Aranthiv
			last_name = yen_Gozhar
			historical = yes
			ruler = yes
			age = 51
			interest_group = ig_armed_forces
			ideology = ideology_theocrat
			traits = {
				meticulous innovative
			}
		}
	}
}
