﻿CHARACTERS = {
	c:B45 ?= {
		create_character = { #actually half-ynnic; daughter of general Camovac Konyrzab and mercenary Arok yen Clovenvels
			first_name = Varhilya
			last_name = Konyrzab
			historical = yes
			ruler = yes
			female = yes
			age = 24
			interest_group = ig_landowners
			ideology = ideology_moderate
			traits = {
				cautious ambitious experienced_political_operator
			}
		}
		create_character = { #the Jester Knight; former Jahanon supporter banished from the Sarda Empire, could be sent back as agitator to destabilize them. Or may die in an Epednar ambush...
			first_name = Fasendir
			last_name = yen_Cestor
			historical = yes
			age = 18
			is_general = yes
			interest_group = ig:ig_intelligentsia
			ideology = ideology_anarchist
			traits = {
				reckless romantic
			}
			# set_home_country = c:B29 #set_home_country isn't used in create_character; not used in vanilla
		}

		create_character = { #half-Ynnic descendant of Jahanon
			first_name = Rarity
			last_name = Ralsork
			culture = east_ynnsman
			religion = ravelian
			female = yes
			historical = yes
			age = 17
			is_agitator = yes
			interest_group = ig:ig_intelligentsia
			ideology = ideology:ideology_authoritarian
			traits = {
				charismatic sickly
			}
			# set_home_country = c:B35 #set_home_country isn't used create_character; not used in vanilla
		}
	}
}
